<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\Message;
use App\Form\Type\CommentaireType;
use App\Form\Type\MessageType;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route(name="article", path="/article/{article}")
     */
    public function view(
        EntityManagerInterface $entityManager,
        Request $request,
        Message $article
    ): Response
    {
        $commentaire = new Commentaire();
        $commentaire->setMessage($article);

        $form = $this->createForm(CommentaireType::class, $commentaire)
            ->add('submit', SubmitType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($commentaire);
            $entityManager->flush();

            return $this->redirectToRoute('article', ['article' => $article->getId()]);
        }

        return $this->render('article_view.html.twig', [
            'article' => $article,
            'form' => $form->createView()
        ]);
    }
}