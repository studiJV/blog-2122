<?php

namespace App\Controller\Admin;

use App\Entity\Message;
use App\Form\Type\MessageType;
use App\Repository\MessageRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route(name="article_liste", path="/admin/article")
     */
    public function list(
        MessageRepository $messageRepository
    ): Response {
        return $this->render('admin/article/liste.html.twig', [
            'liste' => $messageRepository->findAll()
        ]);
    }

    /**
     * @Route(name="article_create", path="/admin/article/new")
     * @Route(name="article_update", path="/admin/article/{message}")
     */
    public function edit(
        EntityManagerInterface $entityManager,
        Request $request,
        Message $message = null
    ): Response {
        $new = $message === null;
        if ($new) {
            $message = new Message();
            $message->setDatePublication(new DateTime());
        }


        $form = $this->createForm(MessageType::class, $message)
            ->add('submit', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message->updateDateModification();
            if ($new) {
                $entityManager->persist($message);
                $this->addFlash('success', 'Le message à bien était créé');
            } else {
                $this->addFlash('success', 'Le message à bien était modifié');
            }
            $entityManager->flush();
            return $this->redirectToRoute('article_liste');
        }

        return $this->render('admin/article/edit.html.twig', ['formMessage' => $form->createView()]);
    }

    /**
     * @Route(name="article_delete", path="/admin/article/{message}/delete")
     */
    public function delete(
        EntityManagerInterface $entityManager,
        Message $message = null
    ): Response {
        $entityManager->remove($message);
        $entityManager->flush();
        return $this->redirectToRoute('article_liste');
    }
}