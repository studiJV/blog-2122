<?php

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MessageRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Message::class);
    }

    public function findMessagePaginer(int $page = 1, int $limit = 5)
    {
        return $this->findBy([], ['datePublication' => 'Desc'], $limit, ($page - 1) * $limit);
    }

    public function findMessagePaginerCount()
    {
        return $this->count([]);
    }
}