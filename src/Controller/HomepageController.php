<?php

namespace App\Controller;

use App\Repository\MessageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route(name="homepage", path="/")
     * @Route(name="paginer", path="/messages/{page}")
     */
    public function home(
        MessageRepository $messageRepository,
        int $page = 1
    ): Response
    {
        $messages = $messageRepository->findMessagePaginer($page);
        $nbMessage = $messageRepository->findMessagePaginerCount();

        return $this->render('homepage.html.twig', [
            'messages' => $messages,
            'currentPage' => $page,
            'maxMessage' => $nbMessage < ($page * 5)
        ]);
    }
}